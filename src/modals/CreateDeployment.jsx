import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import SelectPlateForm from "../components/SelectPlateForm";
import SelectSourceCode from "../components/SelectSourceCode";
import SelectPackageSpring from "../components/SelectPackageSpring";
import SelectDependencySpring from "../components/SelectDependencySpring";
import SelectCredential from "../components/SelectCredential";

export default function CreateDeployment() {
  let [isOpen, setIsOpen] = useState(false);

  const [selected, setSelected] = useState();
  const [selectedSourceCode, setSelectedSourceCode] = useState();
  const [inputToken, setInputToken] = useState(false);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  function selectPlateForm(plateForm) {
    setSelected(plateForm);
  }

  function selectSourceCode(SourceCode) {
    setSelectedSourceCode(SourceCode);
  }

  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center">
        <button
          type="button"
          onClick={openModal}
          className="rounded-md bg-blue-500 px-4 py-3 text-base font-medium text-white focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
        >
          Create Deployment
        </button>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-3xl h-auto transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    Create Deployment
                  </Dialog.Title>

                  {/* Form Create Deployment */}
                  <form className="mt-7 flex flex-col gap-y-7">
                    <div className="grid grid-cols-2 gap-x-5">
                      <div className="flex flex-col gap-y-3">
                        <label>Application Name</label>
                        <input
                          type="text"
                          name="applicationName"
                          placeholder="Enter your application name"
                          className="px-5 py-3 border-2 rounded-lg"
                        />
                      </div>
                      <div className="flex flex-col gap-y-3">
                        <label>Email</label>
                        <input
                          type="text"
                          name="email"
                          placeholder="Enter your email"
                          className="px-5 py-3 border-2 rounded-lg"
                        />
                      </div>
                    </div>
                    <div className="flex flex-col gap-y-3">
                      <label>Description</label>
                      <textarea
                        name="description"
                        cols="30"
                        rows="3"
                        placeholder="Enter your description"
                        className="px-5 py-3 border-2 rounded-lg"
                      ></textarea>
                    </div>
                    <div className="grid grid-cols-2 gap-x-5">
                      <div className="flex flex-col gap-y-3">
                        <label>Select Plateform</label>
                        <SelectPlateForm selectPlateForm={selectPlateForm} />
                      </div>
                      <div className="flex flex-col gap-y-3">
                        <label>Select Source Code</label>
                        <SelectSourceCode selectSourceCode={selectSourceCode} />
                      </div>
                    </div>

                    {/* check select plateform */}
                    {selected === "Spring Boot" ? (
                      <div className="grid grid-cols-2 gap-x-5">
                        <div className="flex flex-col gap-y-3">
                          <label>Choose Packages For Spring</label>
                          <SelectPackageSpring />
                        </div>
                        <div className="flex flex-col gap-y-3">
                          <label>Choose Dependencies For Spring</label>
                          <SelectDependencySpring />
                        </div>
                      </div>
                    ) : null}

                    {/* check select source code */}
                    {selectedSourceCode === "GitHub" ? (
                      <div className="flex flex-col gap-y-7">
                        <div className="flex flex-col gap-y-3">
                          <label>Choose Dependencies For Spring</label>
                          <div className="grid grid-cols-3 items-center gap-x-5">
                            <div className="col-span-2">
                              <SelectCredential />
                            </div>
                            <div className="col-span-1">
                              <button
                                type="button"
                                className="w-full border-2 border-blue-500 text-blue-500 px-5 py-3 rounded-lg"
                                onClick={() => setInputToken(!inputToken)}
                              >
                                Add New Credential
                              </button>
                            </div>
                          </div>
                          <h3 className="text-red-500">
                            To auto deploy your, app please provide us your
                            credential.
                          </h3>

                          {/* check token input */}
                          {inputToken ? (
                            <div className="flex flex-col gap-y-3 mt-[11px]">
                              <label>Your Token</label>
                              <input
                                type="text"
                                name="branch"
                                className="px-5 py-3 border-2 rounded-lg"
                                placeholder="Enter your token"
                              />
                            </div>
                          ) : null}
                        </div>
                        <div className="flex flex-col gap-y-3">
                          <div className="grid grid-cols-3 items-center gap-x-5">
                            <div className="col-span-2 flex flex-col gap-y-3">
                              <label>Your Git RepositoryURL</label>
                              <input
                                type="text"
                                name="branch"
                                className="px-5 py-3 border-2 rounded-lg"
                                placeholder="Enter your git repository url"
                              />
                            </div>
                            <div className="col-span-1 flex flex-col gap-y-3">
                              <label>Git Branch</label>
                              <input
                                type="text"
                                name="branch"
                                className="px-5 py-3 border-2 rounded-lg"
                                placeholder="Enter your branch"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : null}
                  </form>

                  <div className="mt-7 flex justify-end items-center gap-x-5">
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-red-500 px-20 py-3  text-white font-medium"
                      onClick={closeModal}
                    >
                      Cancel
                    </button>
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-blue-500 px-20 py-3 text-white font-medium"
                      onClick={closeModal}
                    >
                      Deploy
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
